function toggleProjectView() {
	AJS.$("#projects-table").toggle();
	AJS.$("#create-folder-link").toggle();
	projectTree.toggle();
}

function createTopLevelFolder() {
	parent = new Object();
	parent.id = "#";
	
	createFolder(projectTree, parent);
}

function createFolder(tree, parent_node) {
	new_node = new Object();
	new_node.type = "folder";
	new_node.name = "New folder";
	
	if (parent_node.id === "#") {
		parent_node_id = 0;
	} else {
		parent_node_id = parent_node.id;
	}

	AJS.$.ajax({
		url: AJS.contextPath() + "/rest/project-hierarchy/1.0/folder/" + parent_node_id,
		contentType: "application/json; charset=utf-8",
		type: "POST",
		data: JSON.stringify(new_node),
		dataType: "json",
		success: function(node) {
			new_node.id = node.ID;
			new_node.text = node.name;
			new_node.children = false;
			new_node.parentID = node.parentID;
			tree.jstree(true).create_node(parent_node, new_node, "last"); 
		},
		error: function (err) {
			alert('Error creating folder.');
		}
	});
}

function deleteFolder(tree, node) {
	AJS.$.ajax({
		url: AJS.contextPath() + "/rest/project-hierarchy/1.0/folder/" + node.id,
		contentType: "application/json; charset=utf-8",
		type: "DELETE",
		success: function() {
			tree.jstree(true).delete_node(node); 
		},
		error: function (err) {
			alert('Error deleting folder.');
		}
	});
}

function renameFolder(tree, node) {
	tree.jstree(true).edit(node);
	tree.on('rename_node.jstree', function(node) { return function(e, data) {
		if (data.node.id == node.id) {
			data.node.name = data.text;
			AJS.$.ajax({
				url: AJS.contextPath() + "/rest/project-hierarchy/1.0/folder/" + node.id,
				contentType: "application/json; charset=utf-8",
				type: "PUT",
				data: "{\"name\" : \"" + data.text + "\"}",
				dataType: "json",
				error: function (err) {
					alert('Error renaming folder.');
				}
			});
			tree.off(e);
		}
	}}(node));
}

function ctxMenu(tree, node) {
	items = new Object();

	if (node.type === "folder") {
		items.createFolder = {
			label: "New folder",
			action: function(tree, node) { return function() { createFolder(tree, node); } }(tree, node)
		};
		
		items.renameFolder = {
			label: "Rename",
			action: function(tree, node) { return function() { renameFolder(tree, node); } }(tree, node)
		};
		
		if (node.children.length == 0) {
			items.deleteFolder = {
				label: "Delete folder",
				action: function(tree, node) { return function() { deleteFolder(tree, node);} }(tree,node)
			};
		}
	} else if (node.type === "repo") {
		links = node.data.links;
		if (links) {
			cloneLinks = links.clone;
			for (ix in cloneLinks) {
				items["copyCloneURL_" + cloneLinks[ix].name] = {
					label: "Copy " + cloneLinks[ix].name + " clone URL",
					action: function(node, link_url) { return function() { window.prompt("Clone URL", link_url); } }(node, cloneLinks[ix].href)
				}
			}
		}
	}
	
	return items;
}

AJS.toInit(function () {
	projectBanner = AJS.$('.project-banners');
	projectTree = AJS.$("<div>");
	projectBanner.after( projectTree );
	projectTree.jstree({
		"core" : {
			"multiple" : false,
			"check_callback" : function (operation, node, node_parent, node_position, more) {
				if (operation === "rename_node") {
					return true;
				} else if (operation === "create_node") {
					return true;
				} else if (operation === "move_node") {
					if (node.type === "repo") {
						return false;
					}
					
					return true;
				} else if (operation === "delete_node") {
					if (node.type === "folder" && node.children.length == 0) {
						return true;
					}
				}
				
				return false;
			},
			"data" : {
				"url" : function (node) {
					if (node.id === "#") {
						return AJS.contextPath() + "/rest/project-hierarchy/1.0/folder/0/child";
					} else if (node.type === "folder") {
						return AJS.contextPath() + "/rest/project-hierarchy/1.0/folder/" + node.id + "/child";
					} else if (node.type === "project") {
						return AJS.contextPath() + "/rest/api/1.0/projects/" + node.data.key + "/repos";
					}
				},
				"dataFilter" : function(data, data_type) {
					data_json = JSON.parse(data);
					if (!AJS.$.isArray(data_json)) {
						nodes = [];
						for (repo_ix in data_json.values) {
							repo = data_json.values[repo_ix];
							node = new Object();
							node.id = "slug" + repo.id;
							node.text = repo.name;
							node.children = false;
							node.icon = "project-hierarchy-repo-node-icon";
							node.type = "repo";
							node.data = repo;
							
							nodes.push(node);
						}
						
						return JSON.stringify(nodes);
					} else {
						for (ix in data_json) {
							node = data_json[ix];
							if (node.type === "project") {
								AJS.$.ajax({
									url : AJS.contextPath() + "/rest/api/1.0/projects/" + node.objKey,
									dataType : 'json',
									contentType: "application/json; charset=utf-8",
									type : 'GET',
									async : false,
									success : function(node) { return function(project) {
											node.data = project;
											node.icon = AJS.contextPath() + "/rest/api/1.0/projects/" + project.key + "/avatar.png?s=32";
											node.name = project.name + " [" + project.key + "]";
											node.li_attr = {"class":"hierarchy-project-item"};
										}}(node)
								});
							}
						}
						return JSON.stringify(data_json);
					}
					
				},
				"success" : function(nodes) {
					if (AJS.$.isArray(nodes)) {
						for (node_ix in nodes) {
							node = nodes[node_ix];
						
							if (!(node.type === "repo")) {
								node.text = node.name;
								node.children = true;
								node.id = node.ID;
								
								if (node.type === "project") {
									//do nothing
								} else if (!(node.type === "repo")) {
									node.type = "folder";
								}
							}
						}
					} else {
						alert("Invalid data received: " + nodes);
					}
				}
			}
		},
		"types" : {
			"#" : {
				"valid_children" : ["project", "folder"]
			},
			"repo" : {
				"valid_children" : []
			},
			"project" : {
				"valid_children" : ["repo"]
			},
			"folder" : {
				"valid_children" : ["project", "folder"]
			}
		},
		"contextmenu" : {
			"select_node" : false,
			"show_at_node" : true,
			"items" : function(tree) { return function(node) { return ctxMenu(tree, node); } }(projectTree)
		},
		"state" : { "filter" : function (k) { delete k.core.selected; return k; } },
		"plugins" : [ "wholerow", "dnd", "types", "contextmenu", "state" ]
	});
	
	
	projectTree.on('select_node.jstree', function (e, data) {
		node = data.node;
		if (node.type === "project") {
			window.location.href = AJS.contextPath() + node.data.link.url;
		} else if (node.type === "repo") {
			window.location.href = AJS.contextPath() + node.data.link.url;
		}
	});
	projectTree.on('move_node.jstree', function (e, data) {
		node = data.node;
		if (node.type === "project" || node.type === "folder") {
			parent_id = data.parent;
			
			parent = projectTree.jstree(true).get_node(parent_id);
			
			if (parent_id === "#") {
				parent_id = 0;
			}
			
			
			child_ix = 0;
			for (c_ix in parent.children) {
				sibling = parent.children[c_ix];
				if (sibling === node.id) {
					child_ix = parseInt(c_ix);
				}
			}
			next_sibling_id = 0;
			if (parent.children.length > child_ix + 1) {
				next_sibling_id = parent.children[child_ix + 1];
			}
			
			AJS.$.ajax({
				url: AJS.contextPath() + "/rest/project-hierarchy/1.0/folder/" + node.id,
				contentType: "application/json; charset=utf-8",
				type: "PUT",
				data: "{\"parentID\": " + parent_id + ", \"beforeID\" : " + next_sibling_id + "}",
				dataType: "json",
				error: function (err) {
					alert('Error moving node.');
				}
			});
		}
	});
	
	AJS.$("#projects-table").hide();
});
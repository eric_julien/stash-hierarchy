package me.ej.plugins.stash;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;

import java.util.ArrayList;
import java.util.List;

import net.java.ao.Query;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.sal.api.transaction.TransactionCallback;

public class NodeServiceImpl implements NodeService {

	private final ActiveObjects ao;
	
	public NodeServiceImpl(final ActiveObjects ao) {
		this.ao = checkNotNull(ao);
	}

	@Override
	public Node create(String name, String type, int parentID) {
		final NodeEntity  nodeEntity = ao.create(NodeEntity.class);
		nodeEntity.setName(name);
		nodeEntity.setParentId(parentID);
		nodeEntity.setType(type);
		orderNode(nodeEntity, 0);
		nodeEntity.save();
		Node folder = new Node(nodeEntity);
		return folder;
	}
	
	@Override
	public Node create(Node node) {
		final NodeEntity nodeEntity = ao.create(NodeEntity.class);
		updateFromNode(nodeEntity, node);
		orderNode(nodeEntity, 0);
		nodeEntity.save();
		return new Node(nodeEntity);
	}


	@Override
	public List<Node> all() {
		List<Node> nodes = new ArrayList<Node>();
		for (NodeEntity nodeEntity : newArrayList(ao.find(NodeEntity.class))) {
			nodes.add(new Node(nodeEntity));
		}
		
 		return nodes;
	}

	@Override
	public Node get(int id) {
		Node node = new Node(ao.get(NodeEntity.class, id));
		return node;
	}

	@Override
	public Node update(int id, Node new_node) {
		NodeEntity entity = ao.get(NodeEntity.class, id);
	
		updateFromNode(entity, new_node);
		
		if (new_node.getBeforeID() != null) {
			orderNode(entity, new_node.getBeforeID());
		}
		
		entity.save();
		
		return new Node(entity);
	}

	private static void updateFromNode(NodeEntity entity, Node node) {
		if (node.getParentID() != null) {
			entity.setParentId(node.getParentID());
		}
		
		if (node.getName() != null) {
			entity.setName(node.getName());
		}
		
		if (node.getType() != null) {
			entity.setType(node.getType());
		}
		
		if (node.getObjKey() != null) {
			entity.setObjKey(node.getObjKey());
		}
	}

	private void orderNode(NodeEntity entity, Integer beforeId) {
		int order;
		if (beforeId != 0) {
			NodeEntity nodeBeforeEntity = ao.get(NodeEntity.class, beforeId);
			order = nodeBeforeEntity.getSortOrder();
		} else {
			NodeEntity[] lastNodeSorted = ao.find(NodeEntity.class, Query.select().where("PARENT_ID = ? AND ID != ?", entity.getParentId(), entity.getID()).order("SORT_ORDER DESC").limit(1));
			if (lastNodeSorted.length == 0) {
				order = 0;
			} else {
				order = lastNodeSorted[0].getSortOrder() + 1;
			}
		}
		
		NodeEntity[] nodesToInc = ao.find(NodeEntity.class, Query.select().where("SORT_ORDER >= ? AND PARENT_ID = ? AND ID != ?",
													   order,
													   entity.getParentId(),
													   entity.getID()));
		for (NodeEntity nodeToInc : nodesToInc) {
			nodeToInc.setSortOrder(nodeToInc.getSortOrder() + 1);
			nodeToInc.save();
		}
		
		entity.setSortOrder(order);
		entity.save();
	}

	@Override
	public void remove(int id) {
		NodeEntity entity = ao.get(NodeEntity.class, id);
		ao.delete(entity);
		
	}

	@Override
	public List<Node> allProjectNodes() {
		List<Node> nodes = new ArrayList<Node>();
		for (NodeEntity nodeEntity : newArrayList(ao.find(NodeEntity.class, Query.select().where("type=?", "project")))) {
			nodes.add(new Node(nodeEntity));
		}
		
 		return nodes;	
 	}

	@Override
	public List<Node> children(int id) {
		ArrayList<NodeEntity> nodeEntityList = newArrayList(ao.find(NodeEntity.class,
															Query.select().where("PARENT_ID=?", id).order("SORT_ORDER ASC")));
		List<Node> nodes = new ArrayList<Node>();
		for (NodeEntity nodeEntity : nodeEntityList) {
			nodes.add(new Node(nodeEntity));
		}
		
 		return nodes;	
 	}

	
}

package me.ej.plugins.stash;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.stash.project.ProjectService;

@Path("/")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class ProjectHierarchyRestService {


	private final NodeService folderService;
	private final ProjectService projectService;


	public ProjectHierarchyRestService(NodeService folderService, ProjectService projectService) {
		this.folderService = checkNotNull(folderService);
		this.projectService = checkNotNull(projectService);
	}

	@GET
	@Path("folder")
	public Response getFolder() {
		updateProjects();
		return Response.ok(folderService.all()).build();
	}
	
	@GET
	@Path("folder/{id}/child")
	public Response getChildrenFolders(@PathParam("id") int id) {
		updateProjects();
		return Response.ok(folderService.children(id)).build();
	}
		
	private void updateProjects() {
		List<Node> projectNodes = folderService.allProjectNodes();
		List<String> projectKeys = projectService.findAllKeys();
		for (String projectKey: projectKeys) {
			Node projectNode = null;
			for (Node curProjectNode: projectNodes) {
				if (curProjectNode.getObjKey() == null) {
					folderService.remove(curProjectNode.getID());
					continue;
				}
				if (curProjectNode.getObjKey().compareTo(projectKey) == 0) {
					projectNode = curProjectNode;
					break;
				}
			}
			
			//If node doesn't exist, create one for root.
			if (projectNode == null) {
				//folderService.create(name, type, parentID)
				projectNode = new Node();
				projectNode.setType("project");
				projectNode.setObjKey(projectKey);
				projectNode.setName(projectService.getByKey(projectKey).getName());
				projectNode.setParentID(0);
				folderService.create(projectNode);
			}
		}
		
	}


	@POST
	@Path("folder") 
	public Response createTopLevelFolder(Node folder) {
		return Response.ok(folderService.create(folder.getName(),
												"",
												0)).build();
	}
	
	@GET
	@Path("folder/{id}")
	public Response getNode(@PathParam("id") int id) {
		return Response.ok(folderService.get(id)).build();
	}
	
	@POST
	@Path("folder/{id}")
	public Response createNode(Node node, @PathParam("id") int id) {
		return Response.ok(folderService.create(node.getName(), null, id)).build();
	}
	
	@PUT
	@Path("folder/{id}")
	public Response updateNode(Node node, @PathParam("id") int id) {
		return Response.ok(folderService.update(id, node)).build();
	}
	
	@DELETE
	@Path("folder/{id}")
	public Response deleteNode(@PathParam("id") int id) {
		folderService.remove(id);
		return Response.ok().build();
	}

}

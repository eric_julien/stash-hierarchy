package me.ej.plugins.stash;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class Node {

	@XmlElement
	private Integer ID;
	
	@XmlElement
	private String name;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement
	private Integer parentID;
	
	
	public Integer getID() {
		return ID;
	}

	public void setID(Integer iD) {
		ID = iD;
	}

	public Integer getParentID() {
		return parentID;
	}

	public void setParentID(Integer parentID) {
		this.parentID = parentID;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getObjKey() {
		return objKey;
	}

	public void setObjKey(String objKey) {
		this.objKey = objKey;
	}
	
	public Integer getBeforeID() {
		return beforeID;
	}
	
	@XmlElement
	private String type;
	
	@XmlElement
	private String objKey;
	
	@XmlElement
	private Integer beforeID;
	
	public Node() { }
	
	public Node(String name, int parentId) {
		this.ID       = -1;
		this.name     = name;
		this.parentID = parentId;
		this.objKey   = null;
		this.type     = "folder";
	}

	public Node(NodeEntity folderEntity) {
		this.ID = folderEntity.getID();
		this.name = folderEntity.getName();
		this.parentID = folderEntity.getParentId();
		this.objKey = folderEntity.getObjKey();
		this.type = folderEntity.getType();
	}



}

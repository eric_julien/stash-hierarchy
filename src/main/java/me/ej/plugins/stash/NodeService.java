package me.ej.plugins.stash;

import java.util.List;

import com.atlassian.activeobjects.tx.Transactional;

@Transactional
public interface NodeService {
	Node create(Node node);
	
	Node create(String name, String type, int parentID);
	
	List<Node> all();
	
	Node get(int id);

	Node update(int id, Node node);

	void remove(int id);

	List<Node> allProjectNodes();

	List<Node> children(int id);
}

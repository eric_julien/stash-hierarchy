package me.ej.plugins.stash;

import net.java.ao.Entity;
import net.java.ao.Preload;

@Preload
public interface NodeEntity extends Entity {
	String getName();
	
	void setName(String name);
	
	int getParentId();
	
	void setParentId(int parentId);
	
	String getObjKey();
	
	void  setObjKey(String objKey);
	
	String getType();
	
	void setType(String type);
	
	Integer getSortOrder();
	
	void setSortOrder(Integer sortOrder);
}

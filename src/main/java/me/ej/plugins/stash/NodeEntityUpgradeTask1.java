package me.ej.plugins.stash;

import net.java.ao.Query;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.activeobjects.external.ActiveObjectsUpgradeTask;
import com.atlassian.activeobjects.external.ModelVersion;

public class NodeEntityUpgradeTask1 implements ActiveObjectsUpgradeTask {

	public NodeEntityUpgradeTask1() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public ModelVersion getModelVersion() {
		return ModelVersion.valueOf("1");
	}

	@Override
	public void upgrade(ModelVersion currentVersion, ActiveObjects ao) {
		ao.migrate(NodeEntity.class);
		
		NodeEntity[] allNodes = ao.find(NodeEntity.class);
		int lastOrder = 0;
		for (NodeEntity node: allNodes) {
			node.setSortOrder(lastOrder);
			node.save();
			lastOrder++;
		}
	}

}
